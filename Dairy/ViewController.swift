//
//  ViewController.swift
//  Dairy
//
//  Created by aya magdy on 4/3/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
  
    @IBOutlet weak var tableView: UITableView!
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext //1
    var notes: [NSManagedObject] = []
    
    private var roundButton = UIButton()

    override func viewDidLoad() {
        print("sdsdsds")
        super.viewDidLoad()
//        tableView.allowsSelection = false
        

    }
    
    // to fetch data from memory
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Note")
        //3
        do {
            notes = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError {
            print("Could not fetch. d\(error), \(error.userInfo)")
        }
        createFloatingButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if roundButton.superview != nil {
            DispatchQueue.main.async {
                self.roundButton.removeFromSuperview()
//                self.roundButton = nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let note = notes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = note.value(forKeyPath: "body") as? String
        return cell
    }
    
    func createFloatingButton() {
        roundButton = UIButton(type: .custom)
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        roundButton.backgroundColor = #colorLiteral(red: 1, green: 0.4896949237, blue: 0.5359896784, alpha: 1)
        // Make sure you replace the name of the image:
        roundButton.setImage(UIImage(named:"pen5"), for: .normal)
        // Make sure to create a function and replace DOTHISONTAP with your own function:
        roundButton.addTarget(self, action: #selector(addBtn), for: UIControl.Event.touchUpInside)
        // We're manipulating the UI, must be on the main thread:
        DispatchQueue.main.async {
            if let keyWindow = UIApplication.shared.keyWindow {
                keyWindow.addSubview(self.roundButton)
                NSLayoutConstraint.activate([
                    keyWindow.trailingAnchor.constraint(equalTo: self.roundButton.trailingAnchor, constant: 15),
                    keyWindow.bottomAnchor.constraint(equalTo: self.roundButton.bottomAnchor, constant: 15),
                    self.roundButton.widthAnchor.constraint(equalToConstant: 75),
                    self.roundButton.heightAnchor.constraint(equalToConstant: 75)])
            }
            // Make the button round:
            self.roundButton.layer.cornerRadius = 37.5
            // Add a black shadow:
            self.roundButton.layer.shadowColor = UIColor.black.cgColor
            self.roundButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
            self.roundButton.layer.masksToBounds = false
            self.roundButton.layer.shadowRadius = 2.0
            self.roundButton.layer.shadowOpacity = 0.5
            
            // Add a pulsing animation to draw attention to button:
            let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.duration = 0.4
            scaleAnimation.repeatCount = .greatestFiniteMagnitude
            scaleAnimation.autoreverses = true
            scaleAnimation.fromValue = 1.0;
            scaleAnimation.toValue = 1.05;
            self.roundButton.layer.add(scaleAnimation, forKey: "scale")
        }
    }
   @objc func addBtn() {
        print("add button ....")
        let alert = UIAlertController(title: "New Note",message: "Add a new Note", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let noteToSave = textField.text else {
                    return
            }
            self.save(noteText: noteToSave)
            self.tableView.reloadData()
        }
    
        let cancelAction = UIAlertAction(title: "Cancel",style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
    
        present(alert, animated: true)
    }
    
    func save(noteText: String) {
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Note", in: managedContext)!
        let note = NSManagedObject(entity: entity, insertInto: managedContext)
        // 3
        note.setValue(noteText, forKeyPath: "body")
        // 4
        do {
            try managedContext.save()
            notes.append(note)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
}

